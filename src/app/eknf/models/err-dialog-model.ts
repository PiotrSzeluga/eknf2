export interface ErrDialogData {
    statusText?: string;
    status?: string;
    code?: number;
    message: string;
    tip?: string;
}
