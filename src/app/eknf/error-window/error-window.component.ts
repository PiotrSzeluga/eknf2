import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ErrDialogData } from '../models/err-dialog-model';
import { TranslateService } from '@ngx-translate/core';

/**
 * Used to display error messages if error occurs
 */
@Component({
  selector: 'app-error-window',
  templateUrl: './error-window.component.html',
  styleUrls: ['./error-window.component.scss']
})
export class ErrorWindowComponent implements OnInit {
  /**
   * Stores dynamically translated error message
   */
  errMessageTranslated = '';
  /**
   * Stores array of known server error responses (regex)
   */
  knownErrors: RegExp[] = [
    /ERR#00/ig,
    /ERR#01/ig,
    /ERR#02/ig,
    /ERR#05/ig,
    /ERR#06/ig,
    /ERR#07/ig,
    /ERR#08/ig
  ];

  constructor(public dialogRef: MatDialogRef<ErrorWindowComponent>, private translate: TranslateService, @Inject(MAT_DIALOG_DATA) public data: ErrDialogData) {}

  /**
   * Matches this.data.message (coming from server response) with coresponding error translation,
   * then uses translate service to perform translation
   * (translations stored in src/assets/i18n/XX.json)
   */
  trMessage() {
    this.errMessageTranslated = this.data.message.match(this.knownErrors[0]) ?
      this.translate.instant('root.err0') :
        this.data.message.match(this.knownErrors[1]) ?
          this.translate.instant('root.err1') :
            this.data.message.match(this.knownErrors[2]) ?
              this.translate.instant('root.err2') :
                this.data.message.match(this.knownErrors[3]) ?
                  this.translate.instant('root.err5') :
                    this.data.message.match(this.knownErrors[4]) ?
                      this.translate.instant('root.err6') :
                        this.data.message.match(this.knownErrors[5]) ?
                          this.translate.instant('root.err7') :
                            this.data.message.match(this.knownErrors[6]) ?
                              this.translate.instant('root.err8') :
                                this.translate.instant('root.errUnknown');
  }

  /**
   * Sets small time interval after which translation is triggered
   */
  ngOnInit() {
    setTimeout(
      () => {
        this.trMessage();
      }, 500);
  }

  /**
   * Closes dialog
   */
  okClicked() {
    this.dialogRef.close();
  }
}
