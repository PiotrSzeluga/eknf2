import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EknfPageComponent } from './eknf-page.component';

describe('EknfPageComponent', () => {
  let component: EknfPageComponent;
  let fixture: ComponentFixture<EknfPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EknfPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EknfPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
