import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { invoLangs } from '../consts/invoice-form-consts';
import { TranslateService } from '@ngx-translate/core';
import { EknfApiService } from '../services/eknf-api.service';
import { MatDialog, MatDialogRef } from '@angular/material';
import { retry, take } from 'rxjs/operators';
import { ErrorWindowComponent } from '../error-window/error-window.component';
import { AcknowledgementComponent } from '../acknowledgement/acknowledgement.component';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";
import { HelpwindowComponent } from '../helpwindow/helpwindow.component';
import { successwindowData } from '../models/succes-dialog-model';
import { SuccesswindowComponent } from '../successwindow/successwindow.component';
import { environment } from '../,,/../../../environments/environment';


@Component({
  selector: 'app-eknf-page',
  templateUrl: './eknf-page.component.html',
  styleUrls: ['./eknf-page.component.scss']
})
export class EknfPageComponent implements OnInit {
  formIsLoading$: Subscription;
  invoiceLanguages: {label: string, value: string}[] = invoLangs;
  inForm: FormGroup;
  pdfURL: any;
  ErrDialogRef: MatDialogRef<ErrorWindowComponent>;
  AckDialogRef: MatDialogRef<AcknowledgementComponent>;
  HelpDialogRef: MatDialogRef<HelpwindowComponent>;
  SuccessDialogRef: MatDialogRef<SuccesswindowComponent>;
  confirmationFlag: any;

  constructor(
    private fBuilder: FormBuilder,
    public translate: TranslateService, 
    public eknfService: EknfApiService, 
    private dialog: MatDialog, 
    private icoReg: MatIconRegistry,
    private dSanitizer: DomSanitizer) {
    translate.addLangs(['en', 'pl']);//, 'de', 'es', 'fr', 'it', 'ru', 'ua', 'hu', 'ja', 'zh', 'ko']);
    //const browserLang = translate.getBrowserLang();
    //translate.use(browserLang.match(/en|de|es|fr|it|ru|ua|hu|ja|zh|ko/) ? browserLang : 'pl');
    //translate.use()
    this.formIsLoading$ = this.eknfService.isLoading.subscribe();
    this.icoReg.addSvgIcon('invoiceCustom',this.dSanitizer.bypassSecurityTrustResourceUrl(`${environment.staticUrl}/assets/icons/lotcom_dane_osobowe.svg`));
  }

  ngOnInit() {
    this.inForm = this.fBuilder.group({
      invoType: new FormControl('', {updateOn: 'change', validators: [Validators.required]}),
      invoLang: new FormControl('', {updateOn: 'change', validators: [Validators.required]}),
      tickets: this.fBuilder.array([this.initTicket()]),
      name: new FormControl('', {updateOn: 'change', validators: [Validators.required, Validators.pattern(/([^0-9]){2,}/i)]}),
      country: new FormControl('', {updateOn: 'change', validators: [Validators.required, Validators.pattern(/([^0-9]){2,}/i)]}),
      city: new FormControl('', {updateOn: 'change', validators: [Validators.required, Validators.pattern(/([^0-9]){2,}/i)]}),
      zipcode: new FormControl('', {updateOn: 'change', validators: [Validators.required, Validators.maxLength(12)]}),
      street: new FormControl('', {updateOn: 'change', validators: [Validators.required, Validators.pattern(/([^0-9]){2,}/i)]}),
      bnumber: new FormControl('', {updateOn: 'change', validators: [Validators.required]}),
      taxid: new FormControl('', {updateOn: 'change', validators: []}),
      phone: new FormControl('', {updateOn: 'change', validators: []}),
      email: new FormControl('', {updateOn: 'change', validators: [Validators.email, Validators.required]}),
      legal: new FormControl('', {updateOn: 'change', validators: [Validators.required]})
    });
    this.inForm.controls.invoType.valueChanges.subscribe(
      val => {
        if(val == 'f'){
          this.inForm.controls.taxid.setValidators(Validators.required);
        }
        else{
          this.inForm.controls.taxid.setValidators(null);
        }
        this.inForm.controls.taxid.updateValueAndValidity();
      }
    );
    console.log(this.inForm.controls.taxid);
  }
  /**
   * @returns New form control for ticket number input
   */
  initTicket() {
    return this.fBuilder.control('', {updateOn: 'change', validators: [Validators.required, Validators.maxLength(10), Validators.minLength(10)]});
  }
  /**
   * Pushes fresh form control (ticket number) to formArray
   */
  addTicket() {
    const control = this.inForm.controls.tickets as FormArray;
    control.push(this.initTicket());
  }
  /**
   * Removes control from formArray (tickets)
   * @param id Index of control to be removed
   */
  removeTicket(id: number) {
    const control = this.inForm.controls.tickets as FormArray;
    control.removeAt(id);
  }
  plusClicked() {
    if (this.inForm.controls.tickets.status === 'VALID') {
      this.addTicket();
    }
  }
  /**
   * Removes ticket field of index id
   * @param id Index of ticket field to be removed
   */
  minusClicked(id: number) {
    this.removeTicket(id);
  }
  /**
   * Forms XML body for http request
   * @param formData Object containing user inputs needed to generate an invoice
   */
  requestBuilder(formData: FormGroup): string {
    const tkts = formData.get('tickets') as FormArray;
    let out = `<invoice_request>
      <customer>
        <name>${formData.controls.name.value}</name>
        <address_line1>${formData.controls.street.value},${formData.controls.bnumber.value}</address_line1>
        <address_line2>${formData.controls.zipcode.value},${formData.controls.city.value},${formData.controls.country.value}</address_line2>
        <vat_id>${formData.controls.taxid.value}</vat_id>
        <email>${formData.controls.email.value}</email>
        <eknf_id>NA</eknf_id>
      </customer>
      <form_of_payment/>
      <language>${formData.controls.invoLang.value}</language>
      <products>`;
    for (let i = 0; i < tkts.length ; i++) {
      out += `<product>
            <type>A</type>
            <number>${tkts.controls[i].value}</number>
          </product>`;
    }
    out += `</products>
    </invoice_request>`;

    return out;
  }

  /**
   * Uses eknf service to send request formed with requestBuilder then handles the response
   * @param formData Object containing user inputs needed to generate an invoice
   */
  submitConfirmed(formData: FormGroup) {
    this.eknfService.isLoading.next(true);
    localStorage.removeItem('pdfURL');
    this.eknfService.sendInvoiceData(this.requestBuilder(formData)).pipe(
      take(1)
    ).subscribe(
      (res) => {
        this.eknfService.isLoading.next(false);
        const jres = this.eknfService.parseXMLresponse(res);
        if (jres.invoice_response.status.match(/OK/i) && jres.invoice_response.hasOwnProperty('pdf_url_tkt')) {
          this.openSuccessDialog({link:jres.invoice_response.pdf_url_tkt});
        } else {
          this.ErrDialogRef = this.dialog.open(ErrorWindowComponent, {width: '80vw', maxWidth: '550px', data: {
            status: 200,
            message: jres.invoice_response.status
          }});
        }
      },
      (err) => {
        this.eknfService.isLoading.next(false);
        this.ErrDialogRef = this.dialog.open(ErrorWindowComponent, {width: '80vw', maxWidth: '550px', data: err});
      }
    );
  }
  /**
   * Displays confirmation window, if confirmed triggers submitConfirmed method
   * @param formData Object containing user inputs needed to generate an invoice
   */
  submitConfirm(formData: FormGroup) {
    this.AckDialogRef = this.dialog.open(AcknowledgementComponent, {width: '80vw', maxWidth: '550px', data: {confirmation: this.confirmationFlag}});
    this.AckDialogRef.afterClosed()
      .pipe(
        take(1)
      )
        .subscribe(
          (result) => {
            if (result.confirmation === true) {
              this.submitConfirmed(formData);
            }
          },
          (err) => {
            this.confirmationFlag = false;
          }
        );
  }
  /**
   * If form has status valid triggers submitConfirm method
   * @param formData Object containing user inputs needed to generate an invoice
   */
  submitClicked(formData: FormGroup) {
    if (formData.status === 'VALID') {
      this.submitConfirm(formData);
    }
  }
  openHelp(){
    this.HelpDialogRef = this.dialog.open(HelpwindowComponent, {width: '80vw', maxWidth: '550px'});
  }
  openSuccessDialog(val:successwindowData){
    this.SuccessDialogRef = this.dialog.open(SuccesswindowComponent, {width:'80vw', maxWidth: '550', data:val})
  }
}
