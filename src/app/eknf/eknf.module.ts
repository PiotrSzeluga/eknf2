import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EknfPageComponent } from './eknf-page/eknf-page.component';
import { EknfApiService } from './services/eknf-api.service';
import { ErrorWindowComponent } from './error-window/error-window.component';
import { AcknowledgementComponent } from './acknowledgement/acknowledgement.component';
import { DeleteWindowComponent } from './delete-window/delete-window.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { MatIconModule } from '@angular/material/icon';
import { CountryCodePipe } from './pipes/country-code.pipe';
import { RecoverComponent } from './recover/recover.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule, MatInputModule } from '@angular/material/';
import { MatDividerModule } from '@angular/material/';
import { MatSelectModule } from '@angular/material/';
import { MatOptionModule } from '@angular/material/';
import { MatButtonModule } from '@angular/material/';
import { MatProgressSpinnerModule } from '@angular/material/';
import { MatCardModule } from '@angular/material/';
import { MatDialogModule } from '@angular/material/';
import { MatExpansionModule } from '@angular/material/';
import { MatRadioModule } from '@angular/material/radio';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { ContactboxComponent } from './contactbox/contactbox.component';
import { HelpwindowComponent } from './helpwindow/helpwindow.component';
import { SuccesswindowComponent } from './successwindow/successwindow.component';

@NgModule({
  declarations: [
    EknfPageComponent,
    CountryCodePipe,
    ErrorWindowComponent,
    AcknowledgementComponent,
    RecoverComponent,
    DeleteWindowComponent,
    ContactboxComponent,
    HelpwindowComponent,
    SuccesswindowComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatSelectModule,
    MatOptionModule,
    MatIconModule,
    MatButtonModule,
    MatDividerModule,
    TranslateModule.forChild(),
    MatProgressSpinnerModule,
    MatDialogModule,
    MatExpansionModule,
    MatCardModule,
    MatRadioModule,
    MatInputModule,
    DropdownModule,
    InputTextModule,
    MatCheckboxModule,
  ],
  exports: [
    EknfPageComponent
  ],
  providers: [
    EknfApiService
  ],
  entryComponents: [
    ErrorWindowComponent, AcknowledgementComponent, DeleteWindowComponent, HelpwindowComponent, SuccesswindowComponent
  ]
})
export class EknfModule { }
