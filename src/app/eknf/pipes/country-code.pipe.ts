import { Pipe, PipeTransform } from '@angular/core';

/**
 * Dictionary containing maping of  iso code to language name
 */
const ccDict = {
  en: 'English',
  pl: 'Polski',
  de: 'Deutsch',
  es: 'Español',
  fr: 'Français',
  it: 'Italiano',
  ru: 'Pусский',
  ua: 'Українська',
  hu: 'Magyar',
  ja: '日本語',
  zh: '中文',
  ko: '한국어'
};


/**
 * Used to translate iso country code to full language name
 */
@Pipe({
  name: 'countryCode'
})
export class CountryCodePipe implements PipeTransform {

  /**
   * @param value iso country code
   * @returns Country name
   */
  transform(value: string): string {
     return ccDict.hasOwnProperty(value.toLowerCase()) ? ccDict[value.toLowerCase()] : 'Other';
  }

}
