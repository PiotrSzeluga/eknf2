import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-contactbox',
  templateUrl: './contactbox.component.html',
  styleUrls: ['./contactbox.component.scss']
})
export class ContactboxComponent implements OnInit {
  wrlangOpen = false;
  splangOpen = false;
  staticUrl = environment.staticUrl;
  constructor(private matReg: MatIconRegistry, private dSan: DomSanitizer) {
    matReg.addSvgIcon('fmessenger',dSan.bypassSecurityTrustResourceUrl(`${environment.staticUrl}/assets/icons/facebook-messenger-brands.svg`))
  }

  ngOnInit() {

  }
  wrlangToggle(){
    this.wrlangOpen = !this.wrlangOpen;
  }
  splangToggle(){
    this.splangOpen = !this.splangOpen;
  }
}
