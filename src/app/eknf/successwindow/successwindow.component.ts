import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-successwindow',
  templateUrl: './successwindow.component.html',
  styleUrls: ['./successwindow.component.scss']
})
export class SuccesswindowComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<SuccesswindowComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.dialogRef.disableClose = true;
  }
  closeClicked() {
    this.dialogRef.close(this.data);
  }

}
