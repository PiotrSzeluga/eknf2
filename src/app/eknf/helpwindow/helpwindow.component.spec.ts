import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HelpwindowComponent } from './helpwindow.component';

describe('HelpwindowComponent', () => {
  let component: HelpwindowComponent;
  let fixture: ComponentFixture<HelpwindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpwindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpwindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
