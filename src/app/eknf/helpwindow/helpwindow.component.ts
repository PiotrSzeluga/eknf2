import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-helpwindow',
  templateUrl: './helpwindow.component.html',
  styleUrls: ['./helpwindow.component.scss']
})
export class HelpwindowComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<HelpwindowComponent> ) { }

  ngOnInit() {}
  okClicked(){
    this.dialogRef.close();
  }

}
