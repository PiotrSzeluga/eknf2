import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const headers =  new HttpHeaders({
    'Content-Type':  'application/xml',
    'x-api-key': environment.API_KEY,
    accept: 'application/xml'
  });

@Injectable({
  providedIn: 'root'
})
export class EknfApiService {
  /**
   * Stores current state of http connection true:active, false:completed / not started
   */
  public isLoading = new BehaviorSubject(false);
  constructor(private htp: HttpClient) { }

  /**
   * Sends http request to BASE_URL
   * @returns http connection observable
   * @param data XML body formed inside invoice form component
   */
  sendInvoiceData(data: string): Observable<any> {
    return this.htp.post<string>(environment.BASE_URL, data, {headers, responseType: 'text' as 'json'});
  }
  /**
   * Transforms xmlstr to dom
   * @param xmlstr xml response recieved from the server
   * @returns Document object model extracted from xmlstr
   */
  parseXMLresponse(xmlstr: string) {
    const parser = new DOMParser();
    const xmdom = parser.parseFromString(xmlstr, 'application/xml');
    return this.xml2json(xmdom);

  }
  /**
   * Transforms DOM to JSON
   * @param srcDOM
   * @returns Dom content as a JSON 
   */
  private xml2json(srcDOM) {
    const children = [...srcDOM.children];
    if (!children.length) {
      return srcDOM.innerHTML;
    }
    const jsonResult = {};
    for (const child of children) {
      const childIsArray = children.filter(eachChild => eachChild.nodeName === child.nodeName).length > 1;
      if (childIsArray) {
        if (jsonResult[child.nodeName] === undefined) {
          jsonResult[child.nodeName] = [this.xml2json(child)];
        } else {
          jsonResult[child.nodeName].push(this.xml2json(child));
        }
      } else {
        jsonResult[child.nodeName] = this.xml2json(child);
      }
    }
    return jsonResult;
  }
}
