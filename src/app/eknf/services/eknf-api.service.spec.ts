import { TestBed } from '@angular/core/testing';

import { EknfApiService } from './eknf-api.service';
import { AppModule } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';

describe('EknfApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      AppModule
    ]
  }));
  it('should be created', () => {
    const service: EknfApiService = TestBed.get(EknfApiService);
    expect(service).toBeTruthy();
  });
});

describe('xml2json', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      AppModule
    ],
  }));
}); 


describe('parseXMLresponse', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      AppModule
    ]
  }));
  it('should be created', () => {
    const service: EknfApiService = TestBed.get(EknfApiService);
    expect(service).toBeTruthy();
  });
  it('should return valid json', () => {
    const service: EknfApiService = TestBed.get(EknfApiService);
    const xmlresponse = `<invoice_request>
    <customer>
      <name>Ździsiu Kolonko</name>
      <address_line1>Mickiewicza. 20/12</address_line1>
      <address_line2>Warszawa 03-345</address_line2>
      <email>Mistrzwudzitsu@onet.pl</email>
    </customer>
    <form_of_payment/>
    <language>PL</language>
    <products>
      <product>
        <type>A</type>
        <number>1234567890</number>
      </product>
    </products>
    </invoice_request>`;
    const ParsedResponse = {invoice_request:{
      customer: {
        name: 'Ździsiu Kolonko',
        address_line1: 'Mickiewicza. 20/12',
        address_line2: 'Warszawa 03-345',
        email: 'Mistrzwudzitsu@onet.pl'
      },
      form_of_payment: '',
      language: 'PL',
        products: { 
         product: { type: 'A', number: '1234567890'} 
        }
      }
    };
    expect(service.parseXMLresponse(xmlresponse)).toEqual(ParsedResponse);
  });
  it('should return valid json 2', () => {
    const service: EknfApiService = TestBed.get(EknfApiService);
    const xmlresponse = `<invoice_request>
    <customer>
      <name></name>
      <address_line1></address_line1>
      <address_line2></address_line2>
      <email></email>
    </customer>
    <form_of_payment/>
    <language></language>
    <products>
      <product>
        <type></type>
        <number></number>
      </product>
    </products>
    </invoice_request>`;
    const ParsedResponse = { 
      invoice_request:{
        customer: {
          name: '',
          address_line1: '',
          address_line2: '',
          email: ''
        },
        form_of_payment: '',
        language: '',
        products: { 
        product: { type: '', number: ''} 
        }
      }
    }
    expect(service.parseXMLresponse(xmlresponse)).toEqual(ParsedResponse);
  });
});