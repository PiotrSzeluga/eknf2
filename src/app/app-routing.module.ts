import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { LotCommonModule } from 'src/lot-common/lot-common.module';


const routes: Routes = [];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    LotCommonModule  
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
