import { Directive, HostListener, Output, ElementRef, EventEmitter, AfterViewInit } from '@angular/core';
import { of } from 'rxjs';

@Directive({
  selector: '[lcmOnHover]'
})
export class OnHoverDirective implements AfterViewInit {

  @Output('activeNavLink') activeNavLink = new EventEmitter<any>()
  private subMenu: any;
  constructor(private elRef: ElementRef) { }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.subMenu = document.getElementById('sub-menu')
  }

  @HostListener('mouseover')
  onHover() {
    const activeNavLinkId = of(this.elRef.nativeElement.id)
    this.activeNavLink.emit(activeNavLinkId)
    this.subMenu.style.display = 'block'
  }

  @HostListener('mouseleave', ["$event"])
  onMouseLeave(event: MouseEvent) {
    if (event.offsetY < 0) {
      this.subMenu.style.display = 'none'
    }
    if (this.elRef.nativeElement.id === "cs" && event.offsetX > 0 && event.offsetY < 35 || this.elRef.nativeElement.id === "book" && event.offsetX < 0 &&event.offsetY < 35) {
      this.subMenu.style.display = 'none'
    }
    this.subMenu.onmouseleave = () => {
      this.subMenu.style.display = 'none'
    }
  }
}
