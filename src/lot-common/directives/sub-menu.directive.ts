import { Directive, ElementRef, Renderer, ViewContainerRef, TemplateRef, Input } from '@angular/core';
import { SubMenu } from '../components/header/sub-menu.interface';

@Directive({
  selector: '[lcmSubMenu]'
})
export class SubMenuDirective {

  constructor(private elRef: ElementRef, private renderer: Renderer, private container: ViewContainerRef, private template: TemplateRef<any>) { }

  @Input() lcmSubMenuOf: Array<SubMenu>;

  ngOnInit(): void {
    this.container.createEmbeddedView(this.template, {$implicit: {
    main: {
      title: "",
      url: ""
    },
    description: "",
    links: [
      {
        title: "",
        url: ""
      }
    ]
  }});
  }
  ngOnChanges() {
    this.container.clear()
    if (this.lcmSubMenuOf) {
      this.lcmSubMenuOf.forEach(element => {
        this.container.createEmbeddedView(this.template, { $implicit: element })
      });
    }
  }
}
