import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SubMenuDirective } from './directives/sub-menu.directive';
import { OnHoverDirective } from './directives/on-hover.directive';
import { MatDialogModule } from '@angular/material/dialog';
import { TokenExpirationDialogComponent } from './components/alerts/token-expiration-dialog/token-expiration-dialog.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    SubMenuDirective,
    OnHoverDirective,
    TokenExpirationDialogComponent
  ],
  entryComponents: [
    TokenExpirationDialogComponent
  ],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    MatMenuModule,
    MatIconModule,
    MatDialogModule,
    HttpClientModule,
    MatButtonModule,
    TranslateModule
  ],
  exports: [
    BrowserAnimationsModule,
    CommonModule,
    MatMenuModule,
    MatIconModule,
    MatDialogModule,
    HeaderComponent,
    FooterComponent,
    SubMenuDirective,
    OnHoverDirective,
    TokenExpirationDialogComponent
  ]
})
export class LotCommonModule { }
