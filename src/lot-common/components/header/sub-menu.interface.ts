export interface SubMenu {
    main: {
        title: string
        url: string
    }
    description?: string
    links?: [
        {
            title: string
            url: string
        }
    ]
}