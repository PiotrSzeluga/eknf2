import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { throwError, Observable, of, forkJoin } from 'rxjs';
import { SubMenu } from './sub-menu.interface';
import { RestApiService } from '../../services/rest.service';
import { DataService } from '../../services/data.service';

/**
 * Komponent wyświetlający header.
 */
@Component({
  selector: 'lcm-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  /**
   * @property {string} language - Przechowuje informacje na temat wybranego języku.
   */
  public language: string = 'pl';
  public subMenu: Observable<any>;
  public navLink$: Observable<SubMenu[]>;

  /**
   * @constructor
   * @private
   * @param translate - Parametr umożliwiający zmianę języka. @see TranslateService
   */
  constructor(private translate: TranslateService, private RestApiService: RestApiService, private DataService: DataService) {
  }
  /**
   * Funkcja zmieniająca język
   * @function
   * @param lang - String określający dany język
   */
  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.initialLanguage();
    this.DataService.getSubMenuData().subscribe(res => {
      this.subMenu = of(res)
    })
  }
  activateNavLink(event: any) {
    this.navLink$ = of((<any>this.subMenu).value[event.value])
  }
  changeLanguage(lang: string) {
    this.language = lang;
    this.translate.use(lang);
    forkJoin(
      this.RestApiService.fetchAirportData(lang),
      this.RestApiService.fetchSubMenuData(lang),
      this.RestApiService.fetchFormFieldData(lang)
    ).subscribe(([airports, submenu, formFields]) => {
      this.DataService.sendAiportData(airports)
      this.DataService.sendSubMenuData(submenu)
      this.DataService.sendFormFieldsData(formFields)
    })
  }
  initialLanguage() {
    let browserLang: string;
    try {
      if (document.referrer.includes('lot.com')) {
        browserLang = document.referrer.slice(document.referrer.indexOf('.com/')+ 8, document.referrer.indexOf('.com/')+ 10)
      } else {
        browserLang = (navigator.languages && navigator.languages.length) ? navigator.languages[0] : navigator['userLanguage'] || navigator.language || navigator['browserLanguage'] || 'en';
      }
    } catch (error) {
      throwError(error)
    } finally {
      if (browserLang.slice(0, 2) === 'pl') {
        this.changeLanguage('pl')
      } else {
        this.changeLanguage('en')
      }
    }
  }
}
