import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TokenExpirationDialogComponent } from './token-expiration-dialog.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('[ Component ] TokenExpirationDialogComponent', () => {
  let component: TokenExpirationDialogComponent;
  let fixture: ComponentFixture<TokenExpirationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TokenExpirationDialogComponent],
      imports: [
        MatDialogModule,
        MatButtonModule,
        HttpClientTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClientTestingModule]
          }
        })
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TokenExpirationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
