import { TestBed } from '@angular/core/testing';

import { RestApiService } from './rest.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialogModule } from '@angular/material/dialog';

describe('[ Service ] RestApiService', () => {
  let httpTestingController: HttpTestingController;
  let service: RestApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, MatDialogModule],
      providers: [RestApiService]
    })
    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.get(RestApiService);
  })

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  describe('fetchCargoData() should:', () => {
    let awbNumber = '09816192'
    let mockResponse = { "_declaration": { "_attributes": { "version": "1.0", "encoding": "utf-8" } }, "Cargo": { "Awb": { "_attributes": { "num": "080-09816192" }, "AwbOrigin": { "_text": "LJU" }, "AwbDestination": { "_text": "YYZ" }, "AwbPieces": { "_text": "1" }, "AwbWeight": { "_text": "7.8" }, "AwbVolume": { "_text": "0.05" }, "Routing": { "line": [{ "_attributes": { "sequence": "1" }, "FlightOrigin": { "_text": "LJU" }, "FlightDest": { "_text": "WAW" }, "FlightNum": { "_text": "LO618" }, "FlightDate": { "_text": "11AUG18" }, "Alloc": { "_text": "KK" }, "Status": { "_text": "RCF" }, "Pieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "FlightCat": { "_text": "P" } }, { "_attributes": { "sequence": "2" }, "FlightOrigin": { "_text": "WAW" }, "FlightDest": { "_text": "YYZ" }, "FlightNum": { "_text": "LO045" }, "FlightDate": { "_text": "12AUG18" }, "Alloc": { "_text": "KK" }, "Status": { "_text": "DLV" }, "Pieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "FlightCat": { "_text": "P" } }] }, "Segments": { "Segment": [{ "_attributes": { "sequence": "1" }, "Origin": { "_text": "LJU" }, "Dest": { "_text": "WAW" }, "FlightNum": { "_text": "LO618" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "11AUG18" }, "ArrivalDate": { "_text": "11AUG18" }, "ArrivalTime": { "_text": "19:33" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.0" }, "Volume": { "_text": "0.06" }, "Status": { "_text": "Booked" }, "EventDate": { "_text": "08AUG18" }, "EventTime": { "_text": "10:24" }, "StatusCode": { "_text": "KK" } }, { "_attributes": { "sequence": "2" }, "Origin": { "_text": "WAW" }, "Dest": { "_text": "YYZ" }, "FlightNum": { "_text": "LO041" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "12AUG18" }, "ArrivalDate": { "_text": "12AUG18" }, "ArrivalTime": { "_text": "17:17" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.0" }, "Volume": { "_text": "0.06" }, "Status": { "_text": "Booked" }, "EventDate": { "_text": "08AUG18" }, "EventTime": { "_text": "10:24" }, "StatusCode": { "_text": "KK" } }, { "_attributes": { "sequence": "3" }, "Origin": { "_text": "LJU" }, "Dest": { "_text": "WAW" }, "FlightNum": { "_text": "LO618" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "11AUG18" }, "ArrivalDate": { "_text": "11AUG18" }, "ArrivalTime": { "_text": "19:33" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Booked" }, "EventDate": { "_text": "10AUG18" }, "EventTime": { "_text": "13:08" }, "StatusCode": { "_text": "KK" } }, { "_attributes": { "sequence": "4" }, "Origin": { "_text": "WAW" }, "Dest": { "_text": "YYZ" }, "FlightNum": { "_text": "LO041" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "12AUG18" }, "ArrivalDate": { "_text": "12AUG18" }, "ArrivalTime": { "_text": "17:17" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Booked" }, "EventDate": { "_text": "10AUG18" }, "EventTime": { "_text": "13:08" }, "StatusCode": { "_text": "KK" } }, { "_attributes": { "sequence": "5" }, "Origin": { "_text": "WAW" }, "Dest": { "_text": "YYZ" }, "FlightNum": { "_text": "LO045" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "12AUG18" }, "ArrivalDate": { "_text": "12AUG18" }, "ArrivalTime": { "_text": "21:05" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Booked" }, "EventDate": { "_text": "10AUG18" }, "EventTime": { "_text": "18:47" }, "StatusCode": { "_text": "NN" } }, { "_attributes": { "sequence": "6" }, "Origin": { "_text": "WAW" }, "Dest": { "_text": "YYZ" }, "FlightNum": { "_text": "LO045" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "12AUG18" }, "ArrivalDate": { "_text": "12AUG18" }, "ArrivalTime": { "_text": "21:05" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Booked" }, "EventDate": { "_text": "10AUG18" }, "EventTime": { "_text": "18:48" }, "StatusCode": { "_text": "KK" } }, { "_attributes": { "sequence": "7" }, "Origin": { "_text": "WAW" }, "Dest": { "_text": "YYZ" }, "FlightNum": { "_text": "LO045" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "12AUG18" }, "ArrivalDate": { "_text": "12AUG18" }, "ArrivalTime": { "_text": "21:05" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Booked" }, "EventDate": { "_text": "10AUG18" }, "EventTime": { "_text": "18:48" }, "StatusCode": { "_text": "KK" } }, { "_attributes": { "sequence": "8" }, "Origin": { "_text": "LJU" }, "Dest": { "_text": "WAW" }, "FlightNum": { "_text": "LO618" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "11AUG18" }, "ArrivalDate": { "_text": "11AUG18" }, "ArrivalTime": { "_text": "19:33" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Departed" }, "EventDate": { "_text": "11AUG18" }, "EventTime": { "_text": "17:36" }, "StatusCode": { "_text": "DEP" } }, { "_attributes": { "sequence": "9" }, "Origin": { "_text": "LJU" }, "Dest": { "_text": "WAW" }, "FlightNum": { "_text": "LO618" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "11AUG18" }, "ArrivalDate": { "_text": "11AUG18" }, "ArrivalTime": { "_text": "19:33" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Arrived" }, "EventDate": { "_text": "11AUG18" }, "EventTime": { "_text": "21:09" }, "StatusCode": { "_text": "ARR" } }, { "_attributes": { "sequence": "10" }, "Origin": { "_text": "LJU" }, "Dest": { "_text": "WAW" }, "FlightNum": { "_text": "LO618" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "11AUG18" }, "ArrivalDate": { "_text": "11AUG18" }, "ArrivalTime": { "_text": "19:33" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Received From Flight" }, "EventDate": { "_text": "11AUG18" }, "EventTime": { "_text": "21:09" }, "StatusCode": { "_text": "RCF" } }, { "_attributes": { "sequence": "11" }, "Origin": { "_text": "LJU" }, "Dest": { "_text": "WAW" }, "FlightNum": { "_text": "LO618" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "11AUG18" }, "ArrivalDate": { "_text": "11AUG18" }, "ArrivalTime": { "_text": "19:33" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Received From Flight" }, "EventDate": { "_text": "11AUG18" }, "EventTime": { "_text": "21:08" }, "StatusCode": { "_text": "RCF" } }, { "_attributes": { "sequence": "12" }, "Origin": { "_text": "LJU" }, "Dest": { "_text": "WAW" }, "FlightNum": { "_text": "LO618" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "11AUG18" }, "ArrivalDate": { "_text": "11AUG18" }, "ArrivalTime": { "_text": "19:33" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Departed on Flight" }, "EventDate": { "_text": "11AUG18" }, "EventTime": { "_text": "22:30" }, "StatusCode": { "_text": "DEP" } }, { "_attributes": { "sequence": "13" }, "Origin": { "_text": "WAW" }, "Dest": { "_text": "YYZ" }, "FlightNum": { "_text": "LO045" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "12AUG18" }, "ArrivalDate": { "_text": "12AUG18" }, "ArrivalTime": { "_text": "21:05" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Pre-Manifested on Flight" }, "EventDate": { "_text": "12AUG18" }, "EventTime": { "_text": "10:27" }, "StatusCode": { "_text": "PRE" } }, { "_attributes": { "sequence": "14" }, "Origin": { "_text": "WAW" }, "Dest": { "_text": "YYZ" }, "FlightNum": { "_text": "LO045" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "12AUG18" }, "ArrivalDate": { "_text": "12AUG18" }, "ArrivalTime": { "_text": "21:05" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Manifested on Flight" }, "EventDate": { "_text": "12AUG18" }, "EventTime": { "_text": "14:51" }, "StatusCode": { "_text": "MAN" } }, { "_attributes": { "sequence": "15" }, "Origin": { "_text": "WAW" }, "Dest": { "_text": "YYZ" }, "FlightNum": { "_text": "LO045" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "12AUG18" }, "ArrivalDate": { "_text": "12AUG18" }, "ArrivalTime": { "_text": "21:05" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Manifested on Flight" }, "EventDate": { "_text": "12AUG18" }, "EventTime": { "_text": "14:54" }, "StatusCode": { "_text": "MAN" } }, { "_attributes": { "sequence": "16" }, "Origin": { "_text": "WAW" }, "Dest": { "_text": "YYZ" }, "FlightNum": { "_text": "LO045" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "12AUG18" }, "ArrivalDate": { "_text": "12AUG18" }, "ArrivalTime": { "_text": "21:05" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Departed on Flight" }, "EventDate": { "_text": "12AUG18" }, "EventTime": { "_text": "18:24" }, "StatusCode": { "_text": "DEP" } }, { "_attributes": { "sequence": "17" }, "Origin": { "_text": "WAW" }, "Dest": { "_text": "YYZ" }, "FlightNum": { "_text": "LO045" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "12AUG18" }, "ArrivalDate": { "_text": "12AUG18" }, "ArrivalTime": { "_text": "21:05" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Departed" }, "EventDate": { "_text": "12AUG18" }, "EventTime": { "_text": "18:24" }, "StatusCode": { "_text": "DEP" } }, { "_attributes": { "sequence": "18" }, "Origin": { "_text": "WAW" }, "Dest": { "_text": "YYZ" }, "FlightNum": { "_text": "LO045" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "12AUG18" }, "ArrivalDate": { "_text": "12AUG18" }, "ArrivalTime": { "_text": "21:05" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Departed on Flight" }, "EventDate": { "_text": "12AUG18" }, "EventTime": { "_text": "17:55" }, "StatusCode": { "_text": "DEP" } }, { "_attributes": { "sequence": "19" }, "Origin": { "_text": "WAW" }, "Dest": { "_text": "YYZ" }, "FlightNum": {}, "FlightCat": {}, "FlightDate": { "_text": "12AUG18" }, "ArrivalDate": {}, "ArrivalTime": {}, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Consignee/Agent notified of arrival" }, "EventDate": { "_text": "12AUG18" }, "EventTime": { "_text": "23:03" }, "StatusCode": { "_text": "NFD" } }, { "_attributes": { "sequence": "20" }, "Origin": { "_text": "WAW" }, "Dest": { "_text": "YYZ" }, "FlightNum": { "_text": "LO045" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "12AUG18" }, "ArrivalDate": { "_text": "12AUG18" }, "ArrivalTime": { "_text": "21:05" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Arrived" }, "EventDate": { "_text": "12AUG18" }, "EventTime": { "_text": "21:05" }, "StatusCode": { "_text": "ARR" } }, { "_attributes": { "sequence": "21" }, "Origin": { "_text": "WAW" }, "Dest": { "_text": "YYZ" }, "FlightNum": { "_text": "LO045" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "12AUG18" }, "ArrivalDate": { "_text": "12AUG18" }, "ArrivalTime": { "_text": "21:05" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Received From Flight" }, "EventDate": { "_text": "12AUG18" }, "EventTime": { "_text": "23:35" }, "StatusCode": { "_text": "RCF" } }, { "_attributes": { "sequence": "22" }, "Origin": { "_text": "WAW" }, "Dest": { "_text": "YYZ" }, "FlightNum": { "_text": "LO045" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "12AUG18" }, "ArrivalDate": { "_text": "12AUG18" }, "ArrivalTime": { "_text": "21:05" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Arrived" }, "EventDate": { "_text": "13AUG18" }, "EventTime": { "_text": "00:44" }, "StatusCode": { "_text": "ARR" } }, { "_attributes": { "sequence": "23" }, "Origin": { "_text": "WAW" }, "Dest": { "_text": "YYZ" }, "FlightNum": { "_text": "LO045" }, "FlightCat": { "_text": "P" }, "FlightDate": { "_text": "12AUG18" }, "ArrivalDate": { "_text": "12AUG18" }, "ArrivalTime": { "_text": "21:05" }, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Received From Flight" }, "EventDate": { "_text": "13AUG18" }, "EventTime": { "_text": "00:44" }, "StatusCode": { "_text": "RCF" } }, { "_attributes": { "sequence": "24" }, "Origin": { "_text": "WAW" }, "Dest": { "_text": "YYZ" }, "FlightNum": {}, "FlightCat": {}, "FlightDate": { "_text": "14AUG18" }, "ArrivalDate": {}, "ArrivalTime": {}, "NumPieces": { "_text": "1" }, "Weight": { "_text": "7.8" }, "Volume": { "_text": "0.05" }, "Status": { "_text": "Delivered" }, "EventDate": { "_text": "14AUG18" }, "EventTime": { "_text": "19:10" }, "StatusCode": { "_text": "DLV" } }] } } } }
    let url = `https://api.lot.com/cargo/parcel/search?awbNumber=080${awbNumber}&forCarrier=LO-V2`
    it('call "GET" method', () => {
      service.fetchCargoData(awbNumber).subscribe(data => { })
      const req = httpTestingController.expectOne(url)
      req.flush(mockResponse)
      httpTestingController.verify();
      expect(req.request.method).toBe('GET')
    })
    
    it('return cargo data', () => {
      service.fetchCargoData(awbNumber).subscribe(data => {
        expect(data).toEqual(mockResponse)
      })
      const req = httpTestingController.expectOne(url)
      req.flush(mockResponse)
      httpTestingController.verify();
    })
  })
  describe('fetchAirportData() should:', () => {
    let mockResponsePL = [
      {
        "airportCode": "AAR",
        "airportName": "Aarhus"
      },
      {
        "airportCode": "ADL",
        "airportName": "Adelaide"
      },
      {
        "airportCode": "AES",
        "airportName": "Alesund"
      },
      {
        "airportCode": "AGH",
        "airportName": "Angelholm"
      },
      {
        "airportCode": "AGP",
        "airportName": "Malaga"
      },
      {
        "airportCode": "AKL",
        "airportName": "Auckland"
      },
      {
        "airportCode": "ALA",
        "airportName": "Ałmaty"
      },
      {
        "airportCode": "ALC",
        "airportName": "Alicante"
      },
      {
        "airportCode": "ALF",
        "airportName": "Alta"
      },
      {
        "airportCode": "AMM",
        "airportName": "Amman"
      },
      {
        "airportCode": "AMS",
        "airportName": "Amsterdam"
      },
      {
        "airportCode": "ARN",
        "airportName": "Sztokholm"
      },
      {
        "airportCode": "ATH",
        "airportName": "Ateny"
      },
      {
        "airportCode": "ATL",
        "airportName": "Atlanta GA"
      },
      {
        "airportCode": "BCN",
        "airportName": "Barcelona"
      },
      {
        "airportCode": "BEG",
        "airportName": "Belgrad"
      },
      {
        "airportCode": "BEY",
        "airportName": "Bejrut"
      },
      {
        "airportCode": "BGO",
        "airportName": "Bergen"
      },
      {
        "airportCode": "BIO",
        "airportName": "Bilbao"
      },
      {
        "airportCode": "BKK",
        "airportName": "Bangkok"
      },
      {
        "airportCode": "BLL",
        "airportName": "Billund"
      },
      {
        "airportCode": "BLQ",
        "airportName": "Bolonia"
      },
      {
        "airportCode": "BNE",
        "airportName": "Brisbane"
      },
      {
        "airportCode": "BOO",
        "airportName": "Bodo"
      },
      {
        "airportCode": "BOS",
        "airportName": "Boston"
      },
      {
        "airportCode": "BRI",
        "airportName": "Bari"
      },
      {
        "airportCode": "BRU",
        "airportName": "Bruksela"
      },
      {
        "airportCode": "BSL",
        "airportName": "Bazylea"
      },
      {
        "airportCode": "BUD",
        "airportName": "Budapeszt"
      },
      {
        "airportCode": "BWN",
        "airportName": "Brunei"
      },
      {
        "airportCode": "BZG",
        "airportName": "Bydgoszcz"
      },
      {
        "airportCode": "CAI",
        "airportName": "Kair"
      },
      {
        "airportCode": "CAN",
        "airportName": "Kanton"
      },
      {
        "airportCode": "CDG",
        "airportName": "Paryż"
      },
      {
        "airportCode": "CFU",
        "airportName": "Korfu"
      },
      {
        "airportCode": "CGK",
        "airportName": "Jakarta"
      },
      {
        "airportCode": "CHC",
        "airportName": "Christchurch"
      },
      {
        "airportCode": "CLE",
        "airportName": "Cleveland OH"
      },
      {
        "airportCode": "CLJ",
        "airportName": "Kluż-Napoka"
      },
      {
        "airportCode": "CLT",
        "airportName": "Charlotte NC"
      },
      {
        "airportCode": "CND",
        "airportName": "Konstanca"
      },
      {
        "airportCode": "CPH",
        "airportName": "Kopenhaga"
      },
      {
        "airportCode": "CTA",
        "airportName": "Katania"
      },
      {
        "airportCode": "DBV",
        "airportName": "Dubrownik"
      },
      {
        "airportCode": "DEL",
        "airportName": "Delhi"
      },
      {
        "airportCode": "DEN",
        "airportName": "Denver CO"
      },
      {
        "airportCode": "DFW",
        "airportName": "Dallas TX"
      },
      {
        "airportCode": "DLC",
        "airportName": "Dalian"
      },
      {
        "airportCode": "DME",
        "airportName": "Moskwa Domodiedowo"
      },
      {
        "airportCode": "DPS",
        "airportName": "Denpasar"
      },
      {
        "airportCode": "DRW",
        "airportName": "Darwin"
      },
      {
        "airportCode": "DUB",
        "airportName": "Dublin"
      },
      {
        "airportCode": "DUS",
        "airportName": "Dusseldorf"
      },
      {
        "airportCode": "ESB",
        "airportName": "Ankara"
      },
      {
        "airportCode": "EVE",
        "airportName": "Harstad-Narwik"
      },
      {
        "airportCode": "EVN",
        "airportName": "Erywań"
      },
      {
        "airportCode": "EWR",
        "airportName": "Nowy Jork - Newark"
      },
      {
        "airportCode": "FAO",
        "airportName": "Faro"
      },
      {
        "airportCode": "FMO",
        "airportName": "Münster"
      },
      {
        "airportCode": "FRA",
        "airportName": "Frankfurt"
      },
      {
        "airportCode": "FUK",
        "airportName": "Fukuoka"
      },
      {
        "airportCode": "GDN",
        "airportName": "Gdańsk"
      },
      {
        "airportCode": "GOA",
        "airportName": "Genua"
      },
      {
        "airportCode": "GOT",
        "airportName": "Göteborg"
      },
      {
        "airportCode": "GRZ",
        "airportName": "Graz"
      },
      {
        "airportCode": "GVA",
        "airportName": "Genewa"
      },
      {
        "airportCode": "HAJ",
        "airportName": "Hanower"
      },
      {
        "airportCode": "HAK",
        "airportName": "Haikou"
      },
      {
        "airportCode": "HAM",
        "airportName": "Hamburg"
      },
      {
        "airportCode": "HAN",
        "airportName": "Hanoi"
      },
      {
        "airportCode": "HEL",
        "airportName": "Helsinki"
      },
      {
        "airportCode": "HGH",
        "airportName": "Hangzhou"
      },
      {
        "airportCode": "HKG",
        "airportName": "Hongkong"
      },
      {
        "airportCode": "HKT",
        "airportName": "Phuket"
      },
      {
        "airportCode": "HRK",
        "airportName": "Charków"
      },
      {
        "airportCode": "IAH",
        "airportName": "Houston"
      },
      {
        "airportCode": "IBZ",
        "airportName": "Ibiza"
      },
      {
        "airportCode": "ICN",
        "airportName": "Seul"
      },
      {
        "airportCode": "IEG",
        "airportName": "Zielona Góra"
      },
      {
        "airportCode": "IEV",
        "airportName": "Kijów - wszystkie lotniska"
      },
      {
        "airportCode": "INN",
        "airportName": "Innsbruck"
      },
      {
        "airportCode": "IST",
        "airportName": "Stambuł"
      },
      {
        "airportCode": "ITM",
        "airportName": "Osaka"
      },
      {
        "airportCode": "JFK",
        "airportName": "Nowy Jork"
      },
      {
        "airportCode": "KBP",
        "airportName": "Kijów-Boryspol"
      },
      {
        "airportCode": "KEF",
        "airportName": "Reykjavík"
      },
      {
        "airportCode": "KGD",
        "airportName": "Kaliningrad"
      },
      {
        "airportCode": "KIV",
        "airportName": "Kiszyniów"
      },
      {
        "airportCode": "KKN",
        "airportName": "Kirkenes"
      },
      {
        "airportCode": "KLR",
        "airportName": "Kalmar"
      },
      {
        "airportCode": "KLU",
        "airportName": "Klagenfurt"
      },
      {
        "airportCode": "KNO",
        "airportName": "Medan"
      },
      {
        "airportCode": "KRK",
        "airportName": "Kraków"
      },
      {
        "airportCode": "KRR",
        "airportName": "Krasnodar"
      },
      {
        "airportCode": "KSC",
        "airportName": "Koszyce"
      },
      {
        "airportCode": "KSU",
        "airportName": "Kristiansund"
      },
      {
        "airportCode": "KTW",
        "airportName": "Katowice"
      },
      {
        "airportCode": "KUF",
        "airportName": "Samara"
      },
      {
        "airportCode": "KUL",
        "airportName": "Kuala Lumpur"
      },
      {
        "airportCode": "KUN",
        "airportName": "Kowno"
      },
      {
        "airportCode": "KZN",
        "airportName": "Kazań"
      },
      {
        "airportCode": "LAS",
        "airportName": "Las Vegas NV"
      },
      {
        "airportCode": "LAX",
        "airportName": "Los Angeles"
      },
      {
        "airportCode": "LCA",
        "airportName": "Larnaka"
      },
      {
        "airportCode": "LCY",
        "airportName": "Londyn - City"
      },
      {
        "airportCode": "LED",
        "airportName": "St Petersburg"
      },
      {
        "airportCode": "LHR",
        "airportName": "Londyn - Heathrow"
      },
      {
        "airportCode": "LIS",
        "airportName": "Lizbona"
      },
      {
        "airportCode": "LJU",
        "airportName": "Lublana"
      },
      {
        "airportCode": "LLA",
        "airportName": "Lulea"
      },
      {
        "airportCode": "LNZ",
        "airportName": "Linz"
      },
      {
        "airportCode": "LON",
        "airportName": "Londyn - wszystkie lotniska"
      },
      {
        "airportCode": "LPA",
        "airportName": "Las Palmas"
      },
      {
        "airportCode": "LUX",
        "airportName": "Luksemburg"
      },
      {
        "airportCode": "LUZ",
        "airportName": "Lublin"
      },
      {
        "airportCode": "LWO",
        "airportName": "Lwów"
      },
      {
        "airportCode": "MAD",
        "airportName": "Madryt"
      },
      {
        "airportCode": "MCO",
        "airportName": "Orlando"
      },
      {
        "airportCode": "MEL",
        "airportName": "Melbourne"
      },
      {
        "airportCode": "MIA",
        "airportName": "Miami"
      },
      {
        "airportCode": "MLA",
        "airportName": "Malta"
      },
      {
        "airportCode": "MMX",
        "airportName": "Malmo"
      },
      {
        "airportCode": "MNL",
        "airportName": "Manila"
      },
      {
        "airportCode": "MOL",
        "airportName": "Molde"
      },
      {
        "airportCode": "MOW",
        "airportName": "Moskwa - wszystkie lotniska"
      },
      {
        "airportCode": "MRS",
        "airportName": "Marsylia"
      },
      {
        "airportCode": "MSP",
        "airportName": "Minneapolis MN"
      },
      {
        "airportCode": "MSQ",
        "airportName": "Mińsk"
      },
      {
        "airportCode": "MUC",
        "airportName": "Monachium"
      },
      {
        "airportCode": "MYJ",
        "airportName": "Matsuyama"
      },
      {
        "airportCode": "NAP",
        "airportName": "Neapol"
      },
      {
        "airportCode": "NCE",
        "airportName": "Nicea"
      },
      {
        "airportCode": "NGO",
        "airportName": "Nagoja"
      },
      {
        "airportCode": "NKG",
        "airportName": "Nankin"
      },
      {
        "airportCode": "NRT",
        "airportName": "Tokio"
      },
      {
        "airportCode": "NUE",
        "airportName": "Norymberga"
      },
      {
        "airportCode": "NYC",
        "airportName": "Nowy Jork - wszystkie lotniska"
      },
      {
        "airportCode": "ODS",
        "airportName": "Odessa"
      },
      {
        "airportCode": "OHD",
        "airportName": "Ochryda"
      },
      {
        "airportCode": "OKA",
        "airportName": "Naha"
      },
      {
        "airportCode": "OLB",
        "airportName": "Olbia"
      },
      {
        "airportCode": "OMO",
        "airportName": "Mostar"
      },
      {
        "airportCode": "OPO",
        "airportName": "Porto"
      },
      {
        "airportCode": "ORD",
        "airportName": "Chicago"
      },
      {
        "airportCode": "OSD",
        "airportName": "Östersund"
      },
      {
        "airportCode": "OSL",
        "airportName": "Oslo"
      },
      {
        "airportCode": "OTP",
        "airportName": "Bukareszt"
      },
      {
        "airportCode": "OZH",
        "airportName": "Zaporoże"
      },
      {
        "airportCode": "PDX",
        "airportName": "Portland OR"
      },
      {
        "airportCode": "PEK",
        "airportName": "Peking"
      },
      {
        "airportCode": "PEN",
        "airportName": "Penang"
      },
      {
        "airportCode": "PER",
        "airportName": "Perth"
      },
      {
        "airportCode": "PHX",
        "airportName": "Phoenix AZ"
      },
      {
        "airportCode": "PIT",
        "airportName": "Pittsburgh PA"
      },
      {
        "airportCode": "PKU",
        "airportName": "Pekanbaru "
      },
      {
        "airportCode": "PLQ",
        "airportName": "Połąga"
      },
      {
        "airportCode": "PMI",
        "airportName": "Palma de Mallorca"
      },
      {
        "airportCode": "PNH",
        "airportName": "Phnom Penh"
      },
      {
        "airportCode": "POZ",
        "airportName": "Poznań"
      },
      {
        "airportCode": "PRG",
        "airportName": "Praga"
      },
      {
        "airportCode": "PRN",
        "airportName": "Pristina"
      },
      {
        "airportCode": "PUY",
        "airportName": "Pula"
      },
      {
        "airportCode": "RGN",
        "airportName": "Rangun"
      },
      {
        "airportCode": "RIX",
        "airportName": "Ryga"
      },
      {
        "airportCode": "RNB",
        "airportName": "Rönneby"
      },
      {
        "airportCode": "RZE",
        "airportName": "Rzeszów"
      },
      {
        "airportCode": "SAN",
        "airportName": "San Diego"
      },
      {
        "airportCode": "SDJ",
        "airportName": "Sendai"
      },
      {
        "airportCode": "SDL",
        "airportName": "Sundsvall"
      },
      {
        "airportCode": "SEA",
        "airportName": "Seattle WA"
      },
      {
        "airportCode": "SFO",
        "airportName": "San Francisco"
      },
      {
        "airportCode": "SFT",
        "airportName": "Skelleftea"
      },
      {
        "airportCode": "SGN",
        "airportName": "Ho Chi Minh"
      },
      {
        "airportCode": "SHA",
        "airportName": "Szanghaj"
      },
      {
        "airportCode": "SIN",
        "airportName": "Singapur"
      },
      {
        "airportCode": "SJJ",
        "airportName": "Sarajewo"
      },
      {
        "airportCode": "SKP",
        "airportName": "Skopje"
      },
      {
        "airportCode": "SOF",
        "airportName": "Sofia"
      },
      {
        "airportCode": "SPU",
        "airportName": "Split"
      },
      {
        "airportCode": "STR",
        "airportName": "Stuttgart"
      },
      {
        "airportCode": "SUB",
        "airportName": "Surabaja"
      },
      {
        "airportCode": "SVG",
        "airportName": "Stavanger"
      },
      {
        "airportCode": "SVO",
        "airportName": "Moskwa-Szeremietiewo"
      },
      {
        "airportCode": "SVQ",
        "airportName": "Sewilla"
      },
      {
        "airportCode": "SVX",
        "airportName": "Jekaterynburg"
      },
      {
        "airportCode": "SYD",
        "airportName": "Sydney"
      },
      {
        "airportCode": "SZG",
        "airportName": "Salzburg"
      },
      {
        "airportCode": "SZX",
        "airportName": "Shenzhen"
      },
      {
        "airportCode": "SZY",
        "airportName": "Olsztyn-Mazury"
      },
      {
        "airportCode": "SZZ",
        "airportName": "Szczecin"
      },
      {
        "airportCode": "TAO",
        "airportName": "Qingdao"
      },
      {
        "airportCode": "TBS",
        "airportName": "Tbilisi"
      },
      {
        "airportCode": "TGD",
        "airportName": "Podgorica"
      },
      {
        "airportCode": "TIA",
        "airportName": "Tirana"
      },
      {
        "airportCode": "TLL",
        "airportName": "Tallinn"
      },
      {
        "airportCode": "TLS",
        "airportName": "Tuluza"
      },
      {
        "airportCode": "TLV",
        "airportName": "Tel Awiw"
      },
      {
        "airportCode": "TOS",
        "airportName": "Tromso"
      },
      {
        "airportCode": "TPA",
        "airportName": "Tampa FL"
      },
      {
        "airportCode": "TPE",
        "airportName": "Tajpej"
      },
      {
        "airportCode": "TRD",
        "airportName": "Trondheim"
      },
      {
        "airportCode": "TRN",
        "airportName": "Turyn"
      },
      {
        "airportCode": "TSE",
        "airportName": "Astana"
      },
      {
        "airportCode": "TSF",
        "airportName": "Treviso"
      },
      {
        "airportCode": "TSR",
        "airportName": "Timisoara"
      },
      {
        "airportCode": "TXL",
        "airportName": "Berlin"
      },
      {
        "airportCode": "UME",
        "airportName": "Umea"
      },
      {
        "airportCode": "VAR",
        "airportName": "Warna"
      },
      {
        "airportCode": "VCE",
        "airportName": "Wenecja"
      },
      {
        "airportCode": "VIE",
        "airportName": "Wiedeń"
      },
      {
        "airportCode": "VLC",
        "airportName": "Walencja"
      },
      {
        "airportCode": "VNO",
        "airportName": "Wilno"
      },
      {
        "airportCode": "WAW",
        "airportName": "Warszawa"
      },
      {
        "airportCode": "WLG",
        "airportName": "Wellington"
      },
      {
        "airportCode": "WRO",
        "airportName": "Wrocław"
      },
      {
        "airportCode": "XMN",
        "airportName": "Xiamen"
      },
      {
        "airportCode": "YEG",
        "airportName": "Edmonton"
      },
      {
        "airportCode": "YHZ",
        "airportName": "Halifax"
      },
      {
        "airportCode": "YMM",
        "airportName": "Fort McMurray"
      },
      {
        "airportCode": "YOW",
        "airportName": "Ottawa"
      },
      {
        "airportCode": "YQB",
        "airportName": "Québec"
      },
      {
        "airportCode": "YUL",
        "airportName": "Montreal"
      },
      {
        "airportCode": "YVR",
        "airportName": "Vancouver BC"
      },
      {
        "airportCode": "YWG",
        "airportName": "Winnipeg MN"
      },
      {
        "airportCode": "YXE",
        "airportName": "Saskatoon"
      },
      {
        "airportCode": "YYC",
        "airportName": "Calgary AL."
      },
      {
        "airportCode": "YYZ",
        "airportName": "Toronto"
      },
      {
        "airportCode": "ZAD",
        "airportName": "Zadar"
      },
      {
        "airportCode": "ZAG",
        "airportName": "Zagrzeb"
      },
      {
        "airportCode": "ZRH",
        "airportName": "Zurych"
      }
    ]
    let mockResponseEN = [
      {
        "airportCode": "AAR",
        "airportName": "Aarhus"
      },
      {
        "airportCode": "ADL",
        "airportName": "Adelaide"
      },
      {
        "airportCode": "AES",
        "airportName": "Alesund"
      },
      {
        "airportCode": "AGH",
        "airportName": "Angelholm"
      },
      {
        "airportCode": "AGP",
        "airportName": "Malaga"
      },
      {
        "airportCode": "AKL",
        "airportName": "Auckland"
      },
      {
        "airportCode": "ALA",
        "airportName": "Almaty"
      },
      {
        "airportCode": "ALC",
        "airportName": "Alicante"
      },
      {
        "airportCode": "ALF",
        "airportName": "Alta"
      },
      {
        "airportCode": "AMM",
        "airportName": "Amman"
      },
      {
        "airportCode": "AMS",
        "airportName": "Amsterdam"
      },
      {
        "airportCode": "ARN",
        "airportName": "Stockholm"
      },
      {
        "airportCode": "ATH",
        "airportName": "Athens"
      },
      {
        "airportCode": "ATL",
        "airportName": "Atlanta"
      },
      {
        "airportCode": "BCN",
        "airportName": "Barcelona"
      },
      {
        "airportCode": "BEG",
        "airportName": "Belgrade"
      },
      {
        "airportCode": "BEY",
        "airportName": "Beirut"
      },
      {
        "airportCode": "BGO",
        "airportName": "Bergen"
      },
      {
        "airportCode": "BIO",
        "airportName": "Bilbao"
      },
      {
        "airportCode": "BKK",
        "airportName": "Bangkok"
      },
      {
        "airportCode": "BLL",
        "airportName": "Billund"
      },
      {
        "airportCode": "BLQ",
        "airportName": "Bologna"
      },
      {
        "airportCode": "BNE",
        "airportName": "Brisbane"
      },
      {
        "airportCode": "BOO",
        "airportName": "Bodo"
      },
      {
        "airportCode": "BOS",
        "airportName": "Boston"
      },
      {
        "airportCode": "BRI",
        "airportName": "Bari"
      },
      {
        "airportCode": "BRU",
        "airportName": "Brussels"
      },
      {
        "airportCode": "BSL",
        "airportName": "Basel"
      },
      {
        "airportCode": "BUD",
        "airportName": "Budapest"
      },
      {
        "airportCode": "BWN",
        "airportName": "Brunei"
      },
      {
        "airportCode": "BZG",
        "airportName": "Bydgoszcz"
      },
      {
        "airportCode": "CAI",
        "airportName": "Cairo"
      },
      {
        "airportCode": "CAN",
        "airportName": "Guangzhou"
      },
      {
        "airportCode": "CDG",
        "airportName": "Paris"
      },
      {
        "airportCode": "CFU",
        "airportName": "Corfu"
      },
      {
        "airportCode": "CGK",
        "airportName": "Jakarta"
      },
      {
        "airportCode": "CHC",
        "airportName": "Christchurch"
      },
      {
        "airportCode": "CLE",
        "airportName": "Cleveland"
      },
      {
        "airportCode": "CLJ",
        "airportName": "Cluj-Napoca"
      },
      {
        "airportCode": "CLT",
        "airportName": "Charlotte"
      },
      {
        "airportCode": "CND",
        "airportName": "Constanta"
      },
      {
        "airportCode": "CPH",
        "airportName": "Copenhagen"
      },
      {
        "airportCode": "CTA",
        "airportName": "Catania"
      },
      {
        "airportCode": "DBV",
        "airportName": "Dubrovnik"
      },
      {
        "airportCode": "DEL",
        "airportName": "Delhi"
      },
      {
        "airportCode": "DEN",
        "airportName": "Denver"
      },
      {
        "airportCode": "DFW",
        "airportName": "Dallas"
      },
      {
        "airportCode": "DLC",
        "airportName": "Dalian"
      },
      {
        "airportCode": "DME",
        "airportName": "Moscow Domodedovo"
      },
      {
        "airportCode": "DPS",
        "airportName": "Denpasar"
      },
      {
        "airportCode": "DRW",
        "airportName": "Darwin"
      },
      {
        "airportCode": "DUB",
        "airportName": "Dublin"
      },
      {
        "airportCode": "DUS",
        "airportName": "Dusseldorf"
      },
      {
        "airportCode": "ESB",
        "airportName": "Ankara"
      },
      {
        "airportCode": "EVE",
        "airportName": "Harstad-Narvik"
      },
      {
        "airportCode": "EVN",
        "airportName": "Yerevan"
      },
      {
        "airportCode": "EWR",
        "airportName": "New York - Newark"
      },
      {
        "airportCode": "FAO",
        "airportName": "Faro"
      },
      {
        "airportCode": "FMO",
        "airportName": ""
      },
      {
        "airportCode": "FRA",
        "airportName": "Frankfurt"
      },
      {
        "airportCode": "FUK",
        "airportName": "Fukuoka"
      },
      {
        "airportCode": "GDN",
        "airportName": "Gdansk"
      },
      {
        "airportCode": "GOA",
        "airportName": "Genoa"
      },
      {
        "airportCode": "GOT",
        "airportName": "Gothenburg"
      },
      {
        "airportCode": "GRZ",
        "airportName": "Graz"
      },
      {
        "airportCode": "GVA",
        "airportName": "Geneva"
      },
      {
        "airportCode": "HAJ",
        "airportName": "Hannover"
      },
      {
        "airportCode": "HAK",
        "airportName": "Haikou"
      },
      {
        "airportCode": "HAM",
        "airportName": "Hamburg"
      },
      {
        "airportCode": "HAN",
        "airportName": "Hanoi"
      },
      {
        "airportCode": "HEL",
        "airportName": "Helsinki"
      },
      {
        "airportCode": "HGH",
        "airportName": "Hangzhou"
      },
      {
        "airportCode": "HKG",
        "airportName": "Hong Kong"
      },
      {
        "airportCode": "HKT",
        "airportName": "Phuket City"
      },
      {
        "airportCode": "HRK",
        "airportName": "Kharkiv"
      },
      {
        "airportCode": "IAH",
        "airportName": "Houston"
      },
      {
        "airportCode": "IBZ",
        "airportName": "Ibiza"
      },
      {
        "airportCode": "ICN",
        "airportName": "Seoul"
      },
      {
        "airportCode": "IEG",
        "airportName": "Zielona Gora"
      },
      {
        "airportCode": "IEV",
        "airportName": "Kiev - all airports"
      },
      {
        "airportCode": "INN",
        "airportName": "Innsbruck"
      },
      {
        "airportCode": "IST",
        "airportName": "Istanbul"
      },
      {
        "airportCode": "ITM",
        "airportName": "Osaka"
      },
      {
        "airportCode": "JFK",
        "airportName": "New York"
      },
      {
        "airportCode": "KBP",
        "airportName": "Kiev-Boryspil"
      },
      {
        "airportCode": "KEF",
        "airportName": "Reykjavík"
      },
      {
        "airportCode": "KGD",
        "airportName": "Kaliningrad"
      },
      {
        "airportCode": "KIV",
        "airportName": "Chisinau"
      },
      {
        "airportCode": "KKN",
        "airportName": "Kirkenes"
      },
      {
        "airportCode": "KLR",
        "airportName": "Kalmar"
      },
      {
        "airportCode": "KLU",
        "airportName": "Klagenfurt"
      },
      {
        "airportCode": "KNO",
        "airportName": "Medan"
      },
      {
        "airportCode": "KRK",
        "airportName": "Krakow"
      },
      {
        "airportCode": "KRR",
        "airportName": "Krasnodar"
      },
      {
        "airportCode": "KSC",
        "airportName": "Kosice"
      },
      {
        "airportCode": "KSU",
        "airportName": "Kristiansund"
      },
      {
        "airportCode": "KTW",
        "airportName": "Katowice"
      },
      {
        "airportCode": "KUF",
        "airportName": "Samara"
      },
      {
        "airportCode": "KUL",
        "airportName": "Kuala Lumpur"
      },
      {
        "airportCode": "KUN",
        "airportName": "Kaunas"
      },
      {
        "airportCode": "KZN",
        "airportName": "Kazan"
      },
      {
        "airportCode": "LAS",
        "airportName": "Las Vegas"
      },
      {
        "airportCode": "LAX",
        "airportName": "Los Angeles"
      },
      {
        "airportCode": "LCA",
        "airportName": "Larnaca"
      },
      {
        "airportCode": "LCY",
        "airportName": "London - City"
      },
      {
        "airportCode": "LED",
        "airportName": "St Petersburg"
      },
      {
        "airportCode": "LHR",
        "airportName": "London - Heathrow"
      },
      {
        "airportCode": "LIS",
        "airportName": "Lisbon"
      },
      {
        "airportCode": "LJU",
        "airportName": "Ljubljana"
      },
      {
        "airportCode": "LLA",
        "airportName": "Lulea"
      },
      {
        "airportCode": "LNZ",
        "airportName": "Linz"
      },
      {
        "airportCode": "LON",
        "airportName": "London - all airports"
      },
      {
        "airportCode": "LPA",
        "airportName": "Las Palmas"
      },
      {
        "airportCode": "LUX",
        "airportName": "Luxembourg"
      },
      {
        "airportCode": "LUZ",
        "airportName": "Lublin"
      },
      {
        "airportCode": "LWO",
        "airportName": "Lviv"
      },
      {
        "airportCode": "MAD",
        "airportName": "Madrid"
      },
      {
        "airportCode": "MCO",
        "airportName": "Orlando"
      },
      {
        "airportCode": "MEL",
        "airportName": "Melbourne"
      },
      {
        "airportCode": "MIA",
        "airportName": "Miami"
      },
      {
        "airportCode": "MLA",
        "airportName": "Malta"
      },
      {
        "airportCode": "MMX",
        "airportName": "Malmo"
      },
      {
        "airportCode": "MNL",
        "airportName": "Manila"
      },
      {
        "airportCode": "MOL",
        "airportName": "Molde"
      },
      {
        "airportCode": "MOW",
        "airportName": "Moscow - all airports"
      },
      {
        "airportCode": "MRS",
        "airportName": "Marseille"
      },
      {
        "airportCode": "MSP",
        "airportName": "Minneapolis"
      },
      {
        "airportCode": "MSQ",
        "airportName": "Minsk"
      },
      {
        "airportCode": "MUC",
        "airportName": "Munich"
      },
      {
        "airportCode": "MXP",
        "airportName": "Milan"
      },
      {
        "airportCode": "MYJ",
        "airportName": "Matsuyama"
      },
      {
        "airportCode": "NAP",
        "airportName": "Naples"
      },
      {
        "airportCode": "NCE",
        "airportName": "Nice"
      },
      {
        "airportCode": "NGO",
        "airportName": "Nagoya"
      },
      {
        "airportCode": "NKG",
        "airportName": "Nanjing"
      },
      {
        "airportCode": "NRT",
        "airportName": "Tokyo"
      },
      {
        "airportCode": "NUE",
        "airportName": "Nuremberg"
      },
      {
        "airportCode": "NYC",
        "airportName": "New York - all airports"
      },
      {
        "airportCode": "ODS",
        "airportName": "Odessa"
      },
      {
        "airportCode": "OHD",
        "airportName": "Ohrid"
      },
      {
        "airportCode": "OKA",
        "airportName": "Okinawa"
      },
      {
        "airportCode": "OLB",
        "airportName": "Olbia"
      },
      {
        "airportCode": "OMO",
        "airportName": "Mostar"
      },
      {
        "airportCode": "OPO",
        "airportName": "Porto"
      },
      {
        "airportCode": "ORD",
        "airportName": "Chicago"
      },
      {
        "airportCode": "OSD",
        "airportName": "Östersund"
      },
      {
        "airportCode": "OSL",
        "airportName": "Oslo"
      },
      {
        "airportCode": "OTP",
        "airportName": "Bucharest"
      },
      {
        "airportCode": "OZH",
        "airportName": "Zaporizhia"
      },
      {
        "airportCode": "PDX",
        "airportName": "Portland"
      },
      {
        "airportCode": "PEK",
        "airportName": "Peking"
      },
      {
        "airportCode": "PEN",
        "airportName": "Penang"
      },
      {
        "airportCode": "PER",
        "airportName": "Perth"
      },
      {
        "airportCode": "PHX",
        "airportName": "Phoenix"
      },
      {
        "airportCode": "PIT",
        "airportName": "Pittsburgh"
      },
      {
        "airportCode": "PKU",
        "airportName": "Pekanbaru"
      },
      {
        "airportCode": "PLQ",
        "airportName": "Palanga"
      },
      {
        "airportCode": "PMI",
        "airportName": "Palma Mallorca"
      },
      {
        "airportCode": "PNH",
        "airportName": "Phnom Penh"
      },
      {
        "airportCode": "POZ",
        "airportName": "Poznan"
      },
      {
        "airportCode": "PRG",
        "airportName": "Prague"
      },
      {
        "airportCode": "PRN",
        "airportName": "Pristina"
      },
      {
        "airportCode": "PUY",
        "airportName": "Pula"
      },
      {
        "airportCode": "RGN",
        "airportName": "Yangon"
      },
      {
        "airportCode": "RIX",
        "airportName": "Riga"
      },
      {
        "airportCode": "RNB",
        "airportName": "Rönneby"
      },
      {
        "airportCode": "RZE",
        "airportName": "Rzeszow"
      },
      {
        "airportCode": "SAN",
        "airportName": "San Diego"
      },
      {
        "airportCode": "SDJ",
        "airportName": "Sendai"
      },
      {
        "airportCode": "SDL",
        "airportName": "Sundsvall"
      },
      {
        "airportCode": "SEA",
        "airportName": "Seattle"
      },
      {
        "airportCode": "SFO",
        "airportName": "San Francisco"
      },
      {
        "airportCode": "SFT",
        "airportName": "Skelleftea"
      },
      {
        "airportCode": "SGN",
        "airportName": "Ho Chi Minh City"
      },
      {
        "airportCode": "SHA",
        "airportName": "Shanghai"
      },
      {
        "airportCode": "SIN",
        "airportName": "Singapore"
      },
      {
        "airportCode": "SJJ",
        "airportName": "Sarajevo"
      },
      {
        "airportCode": "SKP",
        "airportName": "Skopje"
      },
      {
        "airportCode": "SOF",
        "airportName": "Sofia"
      },
      {
        "airportCode": "SPU",
        "airportName": "Split"
      },
      {
        "airportCode": "STR",
        "airportName": "Stuttgart"
      },
      {
        "airportCode": "SUB",
        "airportName": "Surabaya"
      },
      {
        "airportCode": "SVG",
        "airportName": "Stavanger"
      },
      {
        "airportCode": "SVO",
        "airportName": "Moscow Sheremetyevo"
      },
      {
        "airportCode": "SVQ",
        "airportName": "Seville"
      },
      {
        "airportCode": "SVX",
        "airportName": "Yekaterinburg"
      },
      {
        "airportCode": "SYD",
        "airportName": "Sydney"
      },
      {
        "airportCode": "SZG",
        "airportName": "Salzburg"
      },
      {
        "airportCode": "SZX",
        "airportName": "Shenzhen"
      },
      {
        "airportCode": "SZY",
        "airportName": "Olsztyn-Mazury"
      },
      {
        "airportCode": "SZZ",
        "airportName": "Szczecin"
      },
      {
        "airportCode": "TAO",
        "airportName": "Qingdao"
      },
      {
        "airportCode": "TBS",
        "airportName": "Tbilisi"
      },
      {
        "airportCode": "TGD",
        "airportName": "Podgorica"
      },
      {
        "airportCode": "TIA",
        "airportName": "Tirana"
      },
      {
        "airportCode": "TLL",
        "airportName": "Tallinn"
      },
      {
        "airportCode": "TLS",
        "airportName": "Toulouse"
      },
      {
        "airportCode": "TLV",
        "airportName": "Tel Aviv"
      },
      {
        "airportCode": "TOS",
        "airportName": "Tromso"
      },
      {
        "airportCode": "TPA",
        "airportName": "Tampa"
      },
      {
        "airportCode": "TPE",
        "airportName": "Taipei"
      },
      {
        "airportCode": "TRD",
        "airportName": "Trondheim"
      },
      {
        "airportCode": "TRN",
        "airportName": "Turin"
      },
      {
        "airportCode": "TSE",
        "airportName": "Astana"
      },
      {
        "airportCode": "TSF",
        "airportName": "Treviso"
      },
      {
        "airportCode": "TSR",
        "airportName": "Timisoara"
      },
      {
        "airportCode": "TXL",
        "airportName": "Berlin"
      },
      {
        "airportCode": "UME",
        "airportName": "Umea"
      },
      {
        "airportCode": "VAR",
        "airportName": "Varna"
      },
      {
        "airportCode": "VCE",
        "airportName": "Venice"
      },
      {
        "airportCode": "VIE",
        "airportName": "Vienna"
      },
      {
        "airportCode": "VLC",
        "airportName": "Valencia"
      },
      {
        "airportCode": "VNO",
        "airportName": "Vilnius"
      },
      {
        "airportCode": "WAW",
        "airportName": "Warsaw"
      },
      {
        "airportCode": "WLG",
        "airportName": "Wellington"
      },
      {
        "airportCode": "WRO",
        "airportName": "Wroclaw"
      },
      {
        "airportCode": "XMN",
        "airportName": "Xiamen"
      },
      {
        "airportCode": "YEG",
        "airportName": "Edmonton"
      },
      {
        "airportCode": "YHZ",
        "airportName": "Halifax"
      },
      {
        "airportCode": "YMM",
        "airportName": "Fort Mcmurray"
      },
      {
        "airportCode": "YOW",
        "airportName": "Ottawa"
      },
      {
        "airportCode": "YQB",
        "airportName": "Quebec"
      },
      {
        "airportCode": "YUL",
        "airportName": "Montreal"
      },
      {
        "airportCode": "YVR",
        "airportName": "Vancouver"
      },
      {
        "airportCode": "YWG",
        "airportName": "Winnipeg"
      },
      {
        "airportCode": "YXE",
        "airportName": "Saskatoon"
      },
      {
        "airportCode": "YYC",
        "airportName": "Calgary"
      },
      {
        "airportCode": "YYZ",
        "airportName": "Toronto"
      },
      {
        "airportCode": "ZAD",
        "airportName": "Zadar"
      },
      {
        "airportCode": "ZAG",
        "airportName": "Zagreb"
      },
      {
        "airportCode": "ZRH",
        "airportName": "Zurich"
      }
    ]    
    let lang: string
    it('call "GET" method', () => {
      lang = 'pl'
      service.fetchAirportData(lang).subscribe(data => { })
      const req = httpTestingController.expectOne(`./assets/i18n/airports-${lang}.json`)
      req.flush(mockResponsePL)
      httpTestingController.verify();
      expect(req.request.method).toBe('GET')
    })
    it('return airport data in polish', () => {
      lang = 'pl'
      service.fetchAirportData(lang).subscribe((data: any[])=> {
        expect(data).toEqual(mockResponsePL)
        expect(data[data.length -1].airportName).toContain('Zurych')
      })
      const req = httpTestingController.expectOne(`./assets/i18n/airports-${lang}.json`)
      req.flush(mockResponsePL)
      httpTestingController.verify();
    })
    it('return airport data in english', () => {
      lang = 'en'
      service.fetchAirportData(lang).subscribe((data: any[]) => {
        expect(data).toEqual(mockResponseEN)
        expect(data[data.length -1].airportName).toContain('Zurich')
      })
      const req = httpTestingController.expectOne(`./assets/i18n/airports-${lang}.json`)
      req.flush(mockResponseEN)
      httpTestingController.verify();
    })
    
  })
});