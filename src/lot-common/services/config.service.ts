import { Injectable } from '@angular/core';

/**
 * Serwis przechowujący i udostępniający stałe dane
 */
@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  /**
   * Stałe przechowujące dane.
   */
  private authTokenURL: string = 'https://api.lot.com/cargo/auth/token/get';
  private xApiKey: string = 'Pap1jHwWcQ4QVDmXn5ljV7IihNEkQeWhabz62sns';
  private bodyKey: string = 'rCo43Pf0m2Ha0fer';
  private contentType: string = 'application/json';
  constructor() { }

  /**
   * Funkcje udostępniające dane.
   */
  public getAuthTokenURL() {
    return this.authTokenURL
  }
  public getXApiKey() {
    return this.xApiKey
  }
  public getBodyKey() {
    return this.bodyKey
  }
  public getContentType() {
    return this.contentType
  }
}
