import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

/**
 * Serwis przekazujący dane między komponentami.
 */
@Injectable({
  providedIn: 'root'
})
export class DataService {

  /**
   * Zmienne przechowujące dane i tworzące Subjecty
   */
  private aiportDataSubject = new Subject<any>();
  private subMenuSubject = new Subject<any>();
  private formFieldsSubject = new Subject<any>();
  constructor() { }

  /**
   * Funkcje wysyłające dane, czyszczące subskrypcję i gettery.
   */
  sendAiportData(aiportData: any) {
    this.aiportDataSubject.next({ airportData: aiportData})
  }
  sendSubMenuData(subMenuData: any) {
    this.subMenuSubject.next(subMenuData)
  }
  sendFormFieldsData(formFieldsData: any) {
    this.formFieldsSubject.next(formFieldsData)
  }
  clearAiportData() {
    this.aiportDataSubject.next();
  }
  clearSubMenuData() {
    this.subMenuSubject.next();
  }
  clearFormFieldsData() {
    this.formFieldsSubject.next();
  }
  getAiportData(): Observable<any> {
    return this.aiportDataSubject.asObservable();
  }
  getSubMenuData(): Observable<any> {
    return this.subMenuSubject.asObservable();
  }
  getFormFieldsData(): Observable<any> {
    return this.formFieldsSubject.asObservable();
  }
}
