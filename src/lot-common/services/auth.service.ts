import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from "./config.service";
/**
 * Serwis autoryzujący
 */
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  /**
   * @property {string} configContentType - Stała przechwująca informacje o typie wymaganych danych.
   * @property {string} configXApiKey - Stała przechwująca X-Api-Key.
   * @property {string} configBodyKey - Stała przechowująca klucz Body.
   * @property {string} configAuthTokenUrl - Stała przechowująca URL tokenu.
   * @property {string} accessToken - Stała przechowująca token uwierzytelniający.
   * @property {string} tokenType - Stała przechowująca typ tokenu.
   */
  configContentType: string;
  configXApiKey: string;
  configBodyKey: string;
  configAuthTokenUrl: string;
  accessToken: string;
  tokenType: string;

  /**
   * @private
   * @param http - Serwis HTTP {@link HttpClient}.
   * @protected
   * @param ConfigService - Serwis przechowujący adresy, dane. @see {@link ConfigService}
   */
  constructor(private http: HttpClient, protected ConfigService: ConfigService) {
    this.configContentType = ConfigService.getContentType();
    this.configXApiKey = ConfigService.getXApiKey();
    this.configBodyKey = ConfigService.getBodyKey();
    this.configAuthTokenUrl = ConfigService.getAuthTokenURL();
    // this.setAuthToken();
  }
  /**
   * Funckja wysyłająca request o token uwierzytelniający
   * @private
   * @function setAuthToken
   */
  private setAuthToken(): any {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': this.configContentType,
        'X-Api-Key': this.configXApiKey
      })
    }
    this.http.post(this.configAuthTokenUrl, { 'key': this.configBodyKey }, httpOptions).subscribe((auth: { access_token: string, expires_in: number, token_type: string }) => {
      this.accessToken = auth.access_token;
      this.tokenType = auth.token_type;
      this.setTokenRefresh(auth.expires_in);
    })
  }
  /**
   * Funkcja odświeżająca token automatycznie.
   * @private
   * @function setTokenRefresh
   * @param expirationTime - Czas ważności tokenu
   */
  private setTokenRefresh(expirationTime) {
    setTimeout(() => {
      this.setAuthToken();
    }, 1000 * expirationTime - 15000);
  }
  /**
   * Funckja przekazująca token innym komponentom/serwisom
   * @public
   * @function getAuthToken
   */
  public getAuthToken() {
    return `${this.tokenType} ${this.accessToken}`;
  }
}
