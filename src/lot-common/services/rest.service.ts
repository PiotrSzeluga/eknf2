import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from './config.service';
import { MatDialog } from '@angular/material/dialog';
import { TokenExpirationDialogComponent } from '../components/alerts/token-expiration-dialog/token-expiration-dialog.component';
import { environment } from 'src/environments/environment';

declare const accessToken: any;

@Injectable({
  providedIn: 'root'
})
/**
 * Serwis łączący się z backendem.
 */
export class RestApiService {
  private configXApiKey: string;
  private configContentType: string;
  
  constructor(private http: HttpClient, protected ConfigService: ConfigService, private dialog: MatDialog) {
    //this.configXApiKey = ConfigService.getXApiKey();
    //this.configContentType = ConfigService.getContentType();
    //this.refreshSite(accessToken.expires_in);
  }
  public fetchFormFieldData(lang: string) {
    let airportsUrl = `${environment.staticUrl}/assets/i18n/formFields-${lang}.json`;
    return this.http.get(airportsUrl);
  }
  public fetchAirportData(lang: string) {
    let airportsUrl = `${environment.staticUrl}/assets/i18n/airports-${lang}.json`;
    return this.http.get(airportsUrl);
  }
  public fetchSubMenuData(lang: string) {
    let subMenuUrl = `${environment.staticUrl}/assets/i18n/submenu-${lang}.json`;
    return this.http.get(subMenuUrl);
  }
  private refreshSite(expirationTime) {
    setTimeout(() => {
      this.dialog.open(TokenExpirationDialogComponent).afterClosed().subscribe(() => {
        window.location.reload();
      })
    }, 1000 * expirationTime);
  }
}
